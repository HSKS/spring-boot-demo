package com.config;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
public class RedisCacheManager {

    @Resource
    private RedisTemplate redisTemplate;

    public void setWithLive(String key, Object value, long timeout,TimeUnit timeUnit) {
        if (StringUtils.isBlank(key) || !ObjectUtils.anyNotNull(value)) {
            return;
        }
        if (timeout == 0) {
            redisTemplate.opsForValue().set(key, value);
        } else {
            redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
        }

    }

    public void set(String key,String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public boolean expire(Object key,long timeout,TimeUnit timeUnit) {
        return redisTemplate.opsForValue().getOperations().expire(key,timeout,timeUnit);
    }

    public void del(String key) {
        redisTemplate.opsForValue().getOperations().delete(key);
    }
}
