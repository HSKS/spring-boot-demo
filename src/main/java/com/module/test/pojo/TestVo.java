package com.module.test.pojo;

import com.common.VoResponse;

public class TestVo extends VoResponse {
    private String message;

    public TestVo(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
