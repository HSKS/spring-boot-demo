package com.module.test.pojo;

import com.module.test.dao.model.Tests;

public class TestDto extends Tests {
    private int pagenum;

    public int getPagenum() {
        return pagenum;
    }

    public void setPagenum(int pagenum) {
        this.pagenum = pagenum;
    }
}
