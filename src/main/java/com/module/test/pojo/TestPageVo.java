package com.module.test.pojo;

import com.common.VoResponse;
import com.github.pagehelper.PageInfo;
import com.module.test.dao.model.Tests;

public class TestPageVo extends VoResponse {
    private PageInfo<Tests> testPageInfo;

    public PageInfo<Tests> getTestPageInfo() {
        return testPageInfo;
    }

    public void setTestPageInfo(PageInfo<Tests> testPageInfo) {
        this.testPageInfo = testPageInfo;
    }
}
