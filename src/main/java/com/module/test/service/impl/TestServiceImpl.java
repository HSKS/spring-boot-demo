package com.module.test.service.impl;

import com.common.Constants;
import com.common.PageDto;
import com.exception.BusinessException;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.module.test.dao.TestMapper;
import com.module.test.dao.model.Tests;
import com.module.test.pojo.TestPageVo;
import com.module.test.service.ITestService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
@EnableCaching
public class TestServiceImpl implements ITestService {
    @Resource
    private TestMapper testMapper;

    public void hello() {
        System.out.println("hello world");
    }

    @Override
    public boolean add(String testname) {
        Tests tests = new Tests();
        if (StringUtils.isNotEmpty(testname)) {
            tests.setTestname(testname);
            int cnt = testMapper.insertTest(tests);
            return cnt > 0;
        }
        throw new BusinessException("testname 不能为空");
    }

    @Override
    public boolean find(String testname) {
        Tests tests = null;
        if (StringUtils.isNotEmpty(testname)) {
            tests = testMapper.findTestsByTestname(testname);
            return tests != null;
        }
        throw new BusinessException("testname 不能为空");
    }

    @Override
    public TestPageVo page(String testname, int PageNumber) {
        PageDto pageDto = new PageDto();
        Map<String, Object> map = new HashMap<>();
        map.put("testname",testname);
        pageDto.setPageNumber(PageNumber);
        pageDto.setPageSize(Constants.DEFAULT_DATA_PAGE_SIZE);
        Page<Tests> testVoPage = PageHelper.startPage(pageDto).doSelectPage(
                () -> testMapper.getTestPage(map)
        );
        TestPageVo testPageVo = new TestPageVo();
        testPageVo.setTestPageInfo(testVoPage.toPageInfo());
        return testPageVo;
    }
    @Cacheable(key = "'hsk_'+#ag")
    public String testCache(String ag) {
        return "cache";
    }
}
