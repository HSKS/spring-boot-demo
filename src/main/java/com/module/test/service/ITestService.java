package com.module.test.service;

import com.module.test.pojo.TestPageVo;

public interface ITestService {
    void hello();
    boolean add(String testname);
    boolean find(String testname);
    TestPageVo page(String testname, int PageNumber);
    String testCache(String ag);
}
