package com.module.test.dao;

import com.github.pagehelper.Page;
import com.module.test.dao.model.Tests;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

@Mapper
public interface TestMapper {
    Tests findTestsByTestname(@Param("testname") String testname);
    Page getTestPage(Map<String, Object> map);
    int  insertTest(Tests record);
}
