package com.module.test.dao.model;

public class Tests {
    private int id;
    private String testname;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTestname() {
        return testname;
    }

    public void setTestname(String testname) {
        this.testname = testname;
    }

    @Override
    public String toString() {
        return "Tests{" +
                "id=" + id +
                ", testname='" + testname + '\'' +
                '}';
    }
}

