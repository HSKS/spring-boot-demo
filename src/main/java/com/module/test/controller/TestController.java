package com.module.test.controller;

import com.module.test.pojo.TestDto;
import com.module.test.pojo.TestPageVo;
import com.module.test.pojo.TestVo;
import com.module.test.service.ITestService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@EnableCaching
@RequestMapping("/test")
public class TestController {
    private final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Resource
    private ITestService iTestService;

    @ApiOperation(value = "add", notes = "添加tests")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testname",value = "test名称", required = true,dataType = "String")
    })
    @PostMapping("/add")
    @ResponseBody
    public TestVo add(@RequestBody TestDto testDto) {
        boolean flag = iTestService.add(testDto.getTestname());
        if (!flag) {
            return new TestVo("添加失败");
        }
        return new TestVo("添加成功");
    }

    @ApiOperation(value = "findByTestname", notes = "find")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testname",value = "test名称", required = true,dataType = "String")
    })
    @PostMapping("/findByTestname")
    @ResponseBody
    public TestVo findByTestname(@RequestBody TestDto testDto) {
        boolean flag = iTestService.find(testDto.getTestname());
        if (!flag) {
            return new TestVo("添加失败");
        }
        return new TestVo("添加成功");
    }

    @ApiOperation(value = "page", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testname", value = "test名称", required = true, dataType = "String")
    })
    @PostMapping("/page")
    @ResponseBody
    public TestPageVo page(@RequestBody TestDto testDto) {
        return iTestService.page(testDto.getTestname(), testDto.getPagenum());
    }

}
