package com.module.user.service;


import com.module.user.dao.model.Users;
import com.module.user.pojo.UserPageVo;

public interface IUserService {
    Users login(String username, String passwd);
    boolean register(String username, String passwd);
    void batchAdd(String username, String passwd);
    UserPageVo page(String username,int PageNumber);
}