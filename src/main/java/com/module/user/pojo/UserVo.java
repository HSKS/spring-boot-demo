package com.module.user.pojo;

import com.common.VoResponse;
import com.module.user.dao.model.Users;

/**
 * 页面展示内容
 */
public class UserVo extends VoResponse {
    private String message;

    public UserVo(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}