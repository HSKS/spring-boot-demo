package com.module.user.pojo;

import com.common.VoResponse;
import com.github.pagehelper.PageInfo;
import com.module.user.dao.model.Users;

public class UserPageVo extends VoResponse {
    private PageInfo<Users> usersPageInfo;

    public PageInfo<Users> getUsersPageInfo() {
        return usersPageInfo;
    }

    public void setUsersPageInfo(PageInfo<Users> usersPageInfo) {
        this.usersPageInfo = usersPageInfo;
    }
}