package com.module.user.pojo;

import com.module.user.dao.model.Users;

/**
 * 前端传输对象
 */
public class UserDto extends Users {

    private int pagenum;

    public int getPagenum() {
        return pagenum;
    }

    public void setPagenum(int pagenum) {
        this.pagenum = pagenum;
    }
}