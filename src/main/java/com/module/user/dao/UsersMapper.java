package com.module.user.dao;


import com.github.pagehelper.Page;
import com.module.user.dao.model.Users;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

@Mapper
public interface UsersMapper {
    //int deleteByPrimaryKey(Integer id);

   // int insert(Users record);

    int insertSelective(Users record);

    //Users selectByPrimaryKey(Integer id);

    //int updateByPrimaryKeySelective(Users record);

    //int updateByPrimaryKey(Users record);

    //param
    Users findByUsernameAndPasswd(@Param("username") String username, @Param("passwd") String passwd);

    //分页
    Page<Users> getUserPage(Map<String, Object> map);


}