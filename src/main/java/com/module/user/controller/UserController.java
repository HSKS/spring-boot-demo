package com.module.user.controller;


import com.module.user.pojo.UserDto;
import com.module.user.pojo.UserPageVo;
import com.module.user.pojo.UserVo;
import com.module.user.service.IUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


@RestController
@RequestMapping("/user")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Resource
    private IUserService iUserService;

    //swagger2 配置参数 ，访问地址 http://localhost:8080/swagger-ui.html
    @ApiOperation(value="hello", notes="Json格式数据")
    @ApiImplicitParam(name="username", value="用户名", required=false, dataType="String")
    @PostMapping("/hello")
    @ResponseBody
    public UserVo sayHello(@RequestBody UserDto userDto) {
        logger.debug("这是个hello的日志");
        return new UserVo("你好"+userDto.getUsername()) ;
    }

    @ApiOperation(value="登陆", notes="Json格式数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name="username", value="用户名", required=true, dataType="String"),
            @ApiImplicitParam(name="passwd", value="用户密码", required=true, dataType="String")}
    )
    @PostMapping("/login")
    @ResponseBody
    public UserVo login(@RequestBody UserDto userDto) {
        boolean login = iUserService.login(userDto.getUsername(), userDto.getPasswd());
        if(login) {
            return new UserVo("登陆成功");
        }else {
            return new UserVo("登陆失败");
        }
    }

    @ApiOperation(value="注册", notes="Json格式数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name="username", value="用户名", required=true, dataType="String"),
            @ApiImplicitParam(name="passwd", value="用户密码", required=true, dataType="String")}
    )
    @PostMapping("/register")
    @ResponseBody
    public UserVo register(@RequestBody UserDto userDto) {
        boolean login = iUserService.register(userDto.getUsername(), userDto.getPasswd());
        if(login) {
            return new UserVo("注册成功");
        }else {
            return new UserVo("注册失败");
        }
    }

    @PostMapping("/batchAdd")
    @ResponseBody
    public String batchAdd(String username,String passwd) {
        iUserService.batchAdd(username, passwd);
        return "成功";
    }

    @PostMapping("/page")
    @ResponseBody
    public UserPageVo page(@RequestBody UserDto userDto) {
        return iUserService.page(userDto.getUsername(),userDto.getPagenum());
    }
}
