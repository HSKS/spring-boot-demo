package com.crontab;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class Crontabs {

    //每隔五秒执行的定时任务
    @Scheduled(cron = "*/5 * * * * ?")
    public void every5Second() {

    }

    //每隔1分钟执行一次
    @Scheduled(cron = "0 */1 * * * ?")
    public void every1Minute() {

    }

    //每小时执行的定时任务
    @Scheduled(cron = "0 0 */1 * * ?")
    public void hourJob() {

    }

    //每天凌晨执行的定时任务
    @Scheduled(cron = "0 0 0 * * ?")
    public void dayJob() {

    }

    //每天的0点、13点、18点、21点都执行一次
    @Scheduled(cron = "0 0 0,13,18,21 * * ?")
    public void hoursJob() {

    }

//    //每周星期天凌晨1点实行一次
//    @Scheduled(cron = "0 0 * * 0 ?")
//    public void weekJob() {
//
//    }

    //每月1号凌晨1点执行一次
    @Scheduled(cron = "0 0 1 1 * ?")
    public void monthStartJob() {

    }
//
//    //每月最后一天23点执行一次
//    @Scheduled(cron = "0 0 23 L * ?")
//    public void monthEndJob() {
//
//    }
}

