package com.common;

public class Constants {

    /**分页，每页10条**/
    public static int DEFAULT_DATA_PAGE_SIZE = 2;
    /**当前登录用户key**/
    public static String SESSION_USER_KEY = "SESSION_USER";
    /**字符集编码**/
    public static String DEFAULT_ENCODING_UTF8 = "UTF-8";
    /**性别**/
    public static String KEY_SEX ="people_sex";
}
