package com.exception;

import java.io.Serializable;

/**
 * @remark 业务响应异常或校验错误
 */
public class BusinessException extends RuntimeException implements Serializable {

    public BusinessException(String message) {
        super(message);
    }

}