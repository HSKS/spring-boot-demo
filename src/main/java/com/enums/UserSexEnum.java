package com.enums;


import com.common.Constants;

/**
 * 性别枚举类 ---- 多用枚觉少用constants
 */
public enum UserSexEnum {

	SEX_NONE(Constants.KEY_SEX,0,"未明"),
	MAN(Constants.KEY_SEX,1,"男"),
	WOMAN(Constants.KEY_SEX,2,"女");
	
	private String key;
	private Object value;
	private String description;

	UserSexEnum(String key, Object value, String description){
		this(key,value);
		this.description = description;
	}
	UserSexEnum(String key, Object value){
		this.key = key;
		this.value = value;
	}

	/**
	 * 获取性别参数
	 * @param key
	 * @return
	 */
	public static String getValueByKey(Byte key) {
		for (UserSexEnum e : values()) {
			if (e.getKey().equals(key) ) {
				return e.getDescription();
			}
		}
		return SEX_NONE.getDescription();
	}


	public String getKey() {
		return key;
	}
	public Object getValue() {
		return value;
	}
	public String getDescription() {
		return description;
	}

}
