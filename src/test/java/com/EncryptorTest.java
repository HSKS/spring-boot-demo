package com;

import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = App.class)
@RunWith(SpringRunner.class)
public class EncryptorTest {

    String username = "root";
    String password = "123456";

    @Autowired
    StringEncryptor stringEncryptor;

    @Test
    public void testEncryptor() {
        System.out.println("spring.datasource.username:" + stringEncryptor.encrypt(username));
        System.out.println("spring.datasource.password:" + stringEncryptor.encrypt(password));
    }

}
