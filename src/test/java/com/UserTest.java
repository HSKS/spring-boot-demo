package com;



import com.common.PageDto;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.module.user.dao.UsersMapper;
import com.module.user.dao.model.Users;
import com.module.user.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest(classes = App.class)
@RunWith(SpringRunner.class)
public class UserTest {

    @Resource
    private UsersMapper usersMapper;
    @Resource
    private IUserService iUserService;

//    @Test
//    public void testAdd() {
//        Users user = new Users() ;
//        user.setPasswd("123");
//        user.setUsername("bao");
//        usersMapper.insertSelective(user);
//    }


    @Test
    public void testPage() {
        PageDto pageDto = new PageDto();
        Map<String,Object> map=new HashMap<>();
        map.put("username", "enjoy");
        pageDto.setPageNumber(2);//分页页码从1开始
        pageDto.setPageSize(2);//分页条数，
        // 分页查询
        Page<Users> userVoPage = PageHelper.startPage(pageDto).doSelectPage(
                () -> usersMapper.getUserPage(map));
        if (userVoPage.getResult() != null && userVoPage.getResult().size() > 0) {
            userVoPage.toPageInfo().getList().forEach(o -> {
                System.out.println(o.getUsername()+"---"+o.getPasswd());
            });
        }
    }
//
//    @Test
//    public void testFindUser() {
//        Users enjoy = usersMapper.findByUsernameAndPasswd("enjoy", "123");
//        System.out.println(enjoy);
//    }

    //事务支持
//    @Test
//    public void testBathadd() {
//        iUserService.batchAdd("enjoy", "123");
//        System.out.println("------------");
//    }

}
