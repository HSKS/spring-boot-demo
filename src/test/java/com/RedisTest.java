package com;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.config.RedisCacheManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

   @Resource
    private RedisCacheManager rc;


    @Test
    public void testRedis() {
        rc.setWithLive("test", "123", 1L, TimeUnit.MINUTES);
        Object val = rc.get("test");
        assertEquals(val, "123");
        System.out.println(val);
        val = rc.get("321");
        System.out.println(val);
        assertNull(val);
        rc.set("hsk", "桂杰");
        val = rc.get("hsk");
        System.out.println(val);
    }
}
